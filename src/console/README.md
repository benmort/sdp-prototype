SDP Console
===========

The Dockerfile in this directory builds an image that can be used to
launch a bash or iPython shell to connect to the configuration database.
