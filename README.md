
Getting Started
===============

Introduction
------------

Prototype of minimal SDP components required for configuration and execution of
workflows. 

This work is part of the SKA Evolutionary prototype.
