# Common definitions for GitLab CI files

# ============================================================================
# Common definitions for building Python packages
# ============================================================================

.build_python:
  stage: build
  dependencies: []
  image: python:3.7
  artifacts:
    paths:
      - ./$BUILD_PATH/dist/
    expire_in: 1 week

# Executed on a non-master commit

.build_python_dev:
  extends: .build_python
  dependencies: []
  script:
    - cd $BUILD_PATH
    - python3 setup.py egg_info -b+dev.$CI_COMMIT_SHORT_SHA sdist bdist_wheel
  except: [master]

# Executed on a master commit

.build_python_release:
  extends: .build_python
  script:
    - cd $BUILD_PATH
    - python3 setup.py sdist bdist_wheel
  only: [master]

.install_artifacts:
  before_script: # Install from build artifacts
    - pip3 install ska-sdp-config --find-links src/config_db/dist
    - pip3 install ska-sdp-logging --find-links src/logging/dist

# ============================================================================
# Common definitions for publishing Python packages
# ============================================================================

.publish_python_release:
  stage: publish
  only: [master]
  variables:
    TWINE_USERNAME: $TWINE_USERNAME
    TWINE_PASSWORD: $TWINE_PASSWORD
  script:
    - python3 -m pip install twine
    - cd $BUILD_PATH
    - twine upload --repository-url $PYPI_REPOSITORY_URL dist/* || true
    - twine upload --skip-existing -u $PYPI_USER -p $PYPI_PASS dist/* || true

# ============================================================================
# Common definitions for testing Python packages
# ============================================================================

.test_python:
  stage: test
  extends: .install_artifacts
  tags: [docker]
  image: nexus.engageska-portugal.pt/ska-docker/ska-python-buildenv:latest

.test_with_coverage:
  stage: test
  script:
    - cd $BUILD_PATH
    - python3 setup.py test
    # Make the coverage report available at the top level, which will merge it.
    - mv .coverage ../../$CI_JOB_NAME.coverage
    - cp build/reports/unit-tests.xml ../../unit-tests-${CI_JOB_NAME}.xml
    - mv build ../../build
  artifacts:
    paths:
      - ./$CI_JOB_NAME.coverage
      - ./unit-tests-${CI_JOB_NAME}.xml
      - ./build
    expire_in: 1 week
    when: always

# ============================================================================
# Common definitions for building Docker images
# ============================================================================

.build_docker:
  stage: build_docker
  dependencies:
    - build:ska-sdp-logging_dev
    - build:ska-sdp-config_dev
    - build:ska-sdp-logging
    - build:ska-sdp-config
  variables:
    BUILD_REGISTRY_HOST: $CI_REGISTRY/ska-telescope
  tags:
    - docker
    - engageska
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - apk add make git
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - cp src/config_db/dist/*.whl $BUILD_PATH
    - cp src/logging/dist/*.whl $BUILD_PATH
    - cd $BUILD_PATH
    - make DOCKER_REGISTRY_HOST=$BUILD_REGISTRY_HOST build
    - make DOCKER_REGISTRY_HOST=$BUILD_REGISTRY_HOST push

# ============================================================================
# Common definitions for publishing Docker images
# ============================================================================

.push_docker_nexus:
  stage: publish
  only: [master]
  dependencies: []
  variables:
    DOCKER_REGISTRY_HOST: $DOCKER_REGISTRY_HOST
    DOCKER_REGISTRY_USER: $CI_PROJECT_NAME
    GIT_VERSION: ${CI_COMMIT_SHA:0:8}
  tags:
    - docker
    - engageska
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - apk add make git
  script:
    - cd $BUILD_PATH
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - make DOCKER_REGISTRY_HOST=${CI_REGISTRY}/ska-telescope  pull_gitlab
    - docker login -u $DOCKER_REGISTRY_USERNAME -p $DOCKER_REGISTRY_PASSWORD $DOCKER_REGISTRY_HOST
    - make DOCKER_REGISTRY_HOST=$DOCKER_REGISTRY_HOST push_version_gitlab
  retry: 2

# ============================================================================
# Common definitions for uploading results of XRay tests
# ============================================================================

.xray_report:
  stage: publish
  only: [master]
  when: always
  allow_failure: true
  tags: [docker]
  image: python:latest
  script:
    - 'curl -X POST -H "Content-Type: application/json" --fail
         -H "Authorization: Basic $JIRA_AUTH"
         --data @build/cucumber.json
         https://jira.skatelescope.org/rest/raven/1.0/import/execution/cucumber'
  retry: 2 # In case JIRA doesn't work first time
